#include "Buffer.h"
#include "BlendState.h"
#include "Context.h"
#include "DepthStencilState.h"
#include "math_utils.h"
#include "Modeller.h"
#include "RasteriserState.h"
#include "Shader.h"
#include "Texture.h"
#include "VertexArray.h"

// https://github.com/HarryLovesCode/WebAssembly-WebGL-2
#include <emscripten/emscripten.h>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <iostream>
#include <optional>

#define GLFW_INCLUDE_ES3
#include <GLFW/glfw3.h>
#include <GLES3/gl3.h>
#include <stdio.h>
#include <stdlib.h>

GLFWwindow* window;

struct ShaderUniforms
{
    Eigen::Matrix4f mvp;
};

struct PlaygroundTemplate
{
    PlaygroundTemplate()
    {
        texture = utils::Texture("texture.png");

        shader = utils::Shader{"normals.vert.shader", "normals.frag.shader"};
        shader_uniforms = utils::Buffer(utils::Buffer::Type::Uniform);

        shader->set_resource(0, *texture);

        {
            utils::Modeller modeller;
            modeller.draw_cube();
            modeller.set_transform(Eigen::Affine3f(Eigen::Translation<float, 3>(0, 2, 0)));
            modeller.draw_sphere();

            tri_verts = modeller.bake_triangles(
                gsl::make_span(std::vector<utils::Modeller::Semantic>{
                    utils::Modeller::Semantic::Position,
                    utils::Modeller::Semantic::Normal,
                    utils::Modeller::Semantic::TextureCoordinate
                })
            );
        }

        {
            utils::Modeller modeller;

            modeller.begin(utils::Modeller::Lines);
                for (int i = -10; i <= 10; ++i)
                {
                    float brightness = i == 0 ? 1.0f : 0.4f;
                    modeller.normal(brightness,brightness,brightness);
                    modeller.texture(brightness,brightness);
                    modeller.vertex(-10,0,i);
                    modeller.vertex(10,0,i);
                    modeller.vertex(i,0,-10);
                    modeller.vertex(i,0,10);
                }
            modeller.end();


            grid_verts = modeller.bake_lines(
                gsl::make_span(std::vector<utils::Modeller::Semantic>{
                    utils::Modeller::Semantic::Position,
                    utils::Modeller::Semantic::Normal,
                    utils::Modeller::Semantic::TextureCoordinate
                })
            );
        }
    }

    void resize(Eigen::Vector2i new_size)
    {
        viewport_size = new_size;
        glViewport(0, 0, viewport_size[0], viewport_size[1]);
        auto ratio = viewport_size[0] / (float)viewport_size[1];
        projection_matrix = utils::perspective_projection_matrix(45, ratio, 1, 100);
    }

    void render_frame()
    {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        utils::DepthStencilState depth_state{
            utils::DepthStencilState::MaskAll,
            utils::DepthStencilState::DepthTest,
            utils::DepthStencilState::Function::Less,
            0,
            100
        };
        context.bind_depth_stencil_state(depth_state);

        utils::BlendState blend_state{};
        context.bind_blend_state(blend_state);

        utils::RasteriserState rasteriser_state{ utils::RasteriserState::CullMode::CullBack };
        context.bind_rasteriser_state(rasteriser_state);

        auto v = utils::look_at(
            Eigen::Vector3f(10, 5, 10),
            Eigen::Vector3f(0, 0, 0),
            Eigen::Vector3f(0, 1, 0));

        ShaderUniforms uniforms;
        uniforms.mvp = projection_matrix * v * Eigen::Affine3f(Eigen::AngleAxis<float>(glfwGetTime(), Eigen::Vector3f(0, 1, 0))).matrix();
        shader_uniforms->set_data(&uniforms);
        shader->set_uniform(0, &shader_uniforms.value());

        context.bind_shader(*shader);
        context.draw(tri_verts->vertex_array, utils::Context::PrimitiveType::Triangles);

        uniforms.mvp = projection_matrix * v.matrix();
        shader_uniforms->set_data(&uniforms);
        shader->set_uniform(0, &shader_uniforms.value());

        context.bind_shader(*shader);
        context.draw(grid_verts->vertex_array, utils::Context::PrimitiveType::Lines);

    }

    Eigen::Vector2i viewport_size;
    Eigen::Matrix4f projection_matrix;

    utils::Context context;
    std::optional<utils::Shader> shader;

    std::optional<utils::Texture> texture;

    std::optional<utils::Buffer> shader_uniforms;

    std::optional<utils::VertexArrayWithBuffers> tri_verts;
    std::optional<utils::VertexArrayWithBuffers> grid_verts;
};

std::optional<PlaygroundTemplate> playground;

struct Vertex
{
    float x, y, z;
    float r, g, b;
};

static void output_error(int error, const char * msg)
{
    std::cout << "Error: %s\n" << std::endl;
}

static void generate_frame()
{
    int width, height;
    glfwGetFramebufferSize(window, &width, &height);

    auto new_size = Eigen::Vector2i(width, height);
    if (new_size != playground->viewport_size)
    {
        playground->resize(new_size);
    }

    playground->render_frame();

    glfwSwapBuffers(window);
    glfwPollEvents();
}

int main() {
    glfwSetErrorCallback(output_error);

    if (!glfwInit()) {
        fputs("Faileid to initialize GLFW", stderr);
        emscripten_force_exit(EXIT_FAILURE);
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

    window = glfwCreateWindow(1024, 768, "Test Window", NULL, NULL);

    if (!window) {
        fputs("Failed to create GLFW window", stderr);
        glfwTerminate();
        emscripten_force_exit(EXIT_FAILURE);
    }

    glfwMakeContextCurrent(window);

    playground.emplace();

    emscripten_set_main_loop(generate_frame, 0, 0);
}

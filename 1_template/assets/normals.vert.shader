#version 300 es

layout (std140) uniform uniform_0
{
    mat4 mvp;
};

layout(location = 1) in lowp vec3 v_col;
layout(location = 0) in lowp vec3 v_pos;
layout(location = 2) in lowp vec2 v_tex;

out lowp vec2 i_tex;

void main()
{
    gl_Position = mvp * vec4(v_pos, 1.0);
    i_tex = v_tex;
}
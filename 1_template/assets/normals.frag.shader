#version 300 es

in lowp vec2 i_tex;
out lowp vec4 o_color;

uniform sampler2D resource_0;

void main()
{
    o_color = vec4(texture(resource_0, i_tex).rg, 1.0, 1.0);
}
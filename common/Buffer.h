#pragma once

#include <gsl/gsl>

namespace utils
{

class Buffer
{
public:
    enum class Type
    {
        VertexArray,
        IndexArray,
        Uniform
    };

    enum class DataType
    {
        Static,
        Dynamic
    };

    Buffer(Type type);
    Buffer(
        Type buffer_type,
        gsl::span<const gsl::byte> const& data,
        DataType data_type=DataType::Static
    );
    Buffer(Buffer&& oth);
    Buffer& operator=(Buffer&& oth);
    ~Buffer();

    void set_data(
        gsl::span<const gsl::byte> const& data,
        DataType type=DataType::Static
    );

    template <typename T>
    void set_data(
        T* object,
        DataType type=DataType::Static)
    {
      gsl::span<gsl::byte> data_span{
          reinterpret_cast<gsl::byte*>(object),
          reinterpret_cast<gsl::byte*>(object+1)
      };
      set_data(data_span, type);
    }

    Type type() const { return m_type; }

private:
    std::uint32_t m_buffer;
    Type m_type;

    friend class Shader;
    friend class VertexArray;
};
}
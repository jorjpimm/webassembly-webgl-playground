#pragma once

namespace utils
{

class RasteriserState
  {
public:
    enum class CullMode
    {
        CullNone,
        CullBack,
        CullFront,

        CullModeCount
    };

    RasteriserState(
        CullMode cull_mode = CullMode::CullBack
    )
    : m_cull_mode(cull_mode)
    {
    }

private:
  CullMode m_cull_mode;

  friend class Context;
  };

}

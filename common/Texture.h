#pragma once

#include <Eigen/Core>
#include <gsl/gsl>

namespace utils
{

class Resource
{
public:
    Resource();
    Resource(Resource&& oth);
    Resource& operator=(Resource&& oth);
    ~Resource();

protected:
    std::uint32_t m_resource;

    friend class Context;
    friend class Shader;
};

class Texture : public Resource
{
public:
    Texture(gsl::czstring texture_filename);
};


}
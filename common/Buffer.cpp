#include "Buffer.h"

#include <GLES3/gl3.h>

#include <iostream>

namespace utils
{

Buffer::Buffer(Type buffer_type)
: m_type(buffer_type)
{
    glGenBuffers(1, &m_buffer);
}

Buffer::Buffer(Type buffer_type, gsl::span<const gsl::byte> const& data, DataType data_type)
: Buffer(buffer_type)
{
    set_data(data, data_type);
}

Buffer::Buffer(Buffer&& oth)
: m_buffer(0)
{
    std::swap(m_type, oth.m_type);
    std::swap(m_buffer, oth.m_buffer);
}

Buffer& Buffer::operator=(Buffer&& oth)
{
    std::swap(m_type, oth.m_type);
    std::swap(m_buffer, oth.m_buffer);
    return *this;
}

Buffer::~Buffer()
{
    if (glIsBuffer(m_buffer))
    {
        glDeleteBuffers(1, &m_buffer);
    }
}

void Buffer::set_data(gsl::span<const gsl::byte> const& data, DataType data_type)
{
    int gl_type = GL_ARRAY_BUFFER;
    switch(m_type)
    {
    case Type::Uniform:
        gl_type = GL_UNIFORM_BUFFER;
        break;
    case Type::VertexArray:
        gl_type = GL_ARRAY_BUFFER;
        break;
    case Type::IndexArray:
        gl_type = GL_ELEMENT_ARRAY_BUFFER;
        break;
    }

    assert(data.data());

    auto gl_data_type = data_type == DataType::Static ? GL_STATIC_DRAW : GL_DYNAMIC_DRAW;

    glBindBuffer(gl_type, m_buffer);
    glBufferData(gl_type, data.size(), data.data(), gl_data_type);
    glBindBuffer(gl_type, 0);
}


}
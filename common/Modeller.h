#pragma once

#include <Eigen/Core>
#include <Eigen/Geometry>
#include <gsl/gsl>
#include <vector>

namespace utils
{

class Buffer;
class VertexFormat;
class VertexArrayWithBuffers;

class Modeller
{
public:
    enum class Semantic
    {
        Position,
        Colour,
        TextureCoordinate,
        Normal,
    };
    Modeller();
    ~Modeller();

    VertexArrayWithBuffers bake_triangles(
        gsl::span<const Semantic> const& semantics
    ) const;

    VertexArrayWithBuffers bake_lines(
        gsl::span<const Semantic> const& semantics
    ) const;

    void bake_triangles(
        gsl::span<const Semantic> const& semantics,
        gsl::not_null<VertexFormat*> format_out,
        gsl::not_null<Buffer*> index_data_out,
        gsl::not_null<Buffer*> vertex_data_out,
        gsl::not_null<std::size_t*> vertex_count_out) const;

    void bake_lines(
        gsl::span<const Semantic> const& semantics,
        gsl::not_null<VertexFormat*> format_out,
        gsl::not_null<Buffer*> index_data_out,
        gsl::not_null<Buffer*> vertex_data_out,
        gsl::not_null<std::size_t*> vertex_count_out) const;

    // Fixed Functionality GL Emulation
    enum Type { None, Quads, Triangles, Lines };
    void begin( Type = Triangles );
    void end( );

    void vertex(Eigen::Vector3f const&);
    inline void vertex(float x, float y, float z) { vertex( Eigen::Vector3f(x,y,z) ); }
    void normal(Eigen::Vector3f const&);
    inline void normal(float x, float y, float z) { normal( Eigen::Vector3f(x,y,z) ); }
    void texture(Eigen::Vector2f const&);
    inline void texture(float u, float v) { texture( Eigen::Vector2f(u,v) ); }
    void colour(Eigen::Vector4f const &);
    inline void colour(float r, float g, float b, float a = 1.0) { colour( Eigen::Vector4f(r,g,b,a) ); }

    void set_normals_automatic(bool=true);
    bool are_normals_automatic() const;

    // Draw Functions
    void draw_wire_cube(Eigen::Vector3f const& min, Eigen::Vector3f const& size);
    void draw_wire_circle(
        const Eigen::Vector3f &pos,
        const Eigen::Vector3f &normal,
        float radius,
        std::size_t pts=24);

    void draw_sphere(float radius = 1, int lats = 8, int longs = 12);
    void draw_cube(
        const Eigen::Vector3f &horizontal=Eigen::Vector3f(1,0,0),
        const Eigen::Vector3f &vertical=Eigen::Vector3f(0,1,0),
        const Eigen::Vector3f &depth=Eigen::Vector3f(0,0,1),
        float tX=0.0,
        float tY=0.0);
    void draw_quad(
        const Eigen::Vector3f &horizontal=Eigen::Vector3f(1,0,0),
        const Eigen::Vector3f &vertical=Eigen::Vector3f(0,1,0));
    void draw_locator(
        const Eigen::Vector3f &size=Eigen::Vector3f(1,1,1),
        const Eigen::Vector3f &center=Eigen::Vector3f() );

    void set_transform(Eigen::Affine3f const& transform);
    Eigen::Affine3f transform() const;

    void save();
    void restore();

private:
    void bake_vertices(
        gsl::span<const Semantic> const& semantics,
        gsl::not_null<VertexFormat*> format_out,
        gsl::not_null<Buffer*> vertex_data,
        std::size_t* vertex_count) const;

    inline Eigen::Vector3f transform_point(Eigen::Vector3f const&);
    inline void transform_points(std::vector<Eigen::Vector3f> &);

    inline Eigen::Vector3f transform_normal(Eigen::Vector3f const&);
    inline void transform_normals(std::vector<Eigen::Vector3f> &, bool reNormalize);

    bool m_are_triangle_indices_sequential;
    bool m_are_line_indices_sequential;

    std::vector<std::uint16_t> m_tri_indices;
    std::vector<std::uint16_t> m_line_indices;

    std::vector<Eigen::Vector3f> m_vertex;
    std::vector<Eigen::Vector2f> m_texture;
    std::vector<Eigen::Vector3f> m_normals;
    std::vector<Eigen::Vector4f> m_colours;

    struct State
    {
        State()
        : normal(Eigen::Vector3f::Zero())
        , texture(Eigen::Vector2f::Zero())
        , colour(0.0f, 0.0f, 0.0f, 0.0f)
        , type(None)
        , normals_automatic( false )
        {
        }

        Eigen::Vector3f normal;
        Eigen::Vector2f texture;
        Eigen::Vector4f colour;
        Type type;
        bool normals_automatic;
    };
    std::vector<State> m_states;

    Eigen::Affine3f m_transform;
    int m_quad_count;
};

}

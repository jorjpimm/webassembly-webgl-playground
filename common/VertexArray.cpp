#include "VertexArray.h"
#include "Buffer.h"

#include <GLES3/gl3.h>

namespace utils
{

namespace {
int gl_vertex_type(utils::VertexAttribute::Type type)
{
    switch(type)
    {
    case utils::VertexAttribute::Type::Byte: return GL_BYTE;
    case utils::VertexAttribute::Type::UnsignedByte: return GL_UNSIGNED_BYTE;
    case utils::VertexAttribute::Type::Short: return GL_SHORT;
    case utils::VertexAttribute::Type::UnsignedShort: return GL_UNSIGNED_SHORT;
    case utils::VertexAttribute::Type::Int: return GL_INT;
    case utils::VertexAttribute::Type::UnsignedInt: return GL_UNSIGNED_INT;
    case utils::VertexAttribute::Type::HalfFloat: return GL_HALF_FLOAT;
    case utils::VertexAttribute::Type::Float: return GL_FLOAT;
    case utils::VertexAttribute::Type::Fixed: return GL_FIXED;
    case utils::VertexAttribute::Type::UnsignedInt_2_10_10_10: return GL_UNSIGNED_INT_2_10_10_10_REV;
    case utils::VertexAttribute::Type::Int_2_10_10_10: return GL_INT_2_10_10_10_REV;
    }
}
}

VertexAttribute::VertexAttribute(
    Type type,
    std::size_t count,
    std::size_t stride,
    std::size_t offset,
    bool normalised)
: count(count)
, type(type)
, normalised(normalised)
, stride(stride)
, offset(offset)
{
    assert(count > 0 && count <= 4);
}

VertexFormat::VertexFormat(std::initializer_list<VertexAttribute> const& attrs)
: m_attributes(attrs)
{
}

VertexFormat::VertexFormat(std::vector<VertexAttribute> const& attrs)
: m_attributes(attrs)
{
}

VertexArray::VertexArray(
    utils::VertexFormat const& format,
    utils::Buffer const& buffer,
    std::size_t vertex_count)
: m_vertex_count(vertex_count)
, m_indexed(false)
{
    assert(buffer.m_type == utils::Buffer::Type::VertexArray);
    glGenVertexArrays(1, &m_array);
    glBindVertexArray(m_array);

    glBindBuffer(GL_ARRAY_BUFFER, buffer.m_buffer);

    std::uint32_t index = 0;
    for (auto const& attr : format.attributes())
    {
        glEnableVertexAttribArray(index);
        glVertexAttribPointer(
            index,
            attr.count,
            gl_vertex_type(attr.type),
            attr.normalised ? GL_TRUE : GL_FALSE,
            attr.stride,
            (void*)attr.offset
        );

        index += 1;
    }

    glBindVertexArray(0);
}

VertexArray::VertexArray(
    utils::VertexFormat const& format,
    utils::Buffer const& index_buffer,
    utils::Buffer const& buffer,
    std::size_t vertex_count)
: m_vertex_count(vertex_count)
, m_indexed(true)
{
    assert(index_buffer.m_type == utils::Buffer::Type::IndexArray);
    assert(buffer.m_type == utils::Buffer::Type::VertexArray);

    glGenVertexArrays(1, &m_array);
    glBindVertexArray(m_array);

    glBindBuffer(GL_ARRAY_BUFFER, buffer.m_buffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer.m_buffer);

    std::uint32_t index = 0;
    for (auto const& attr : format.attributes())
    {
        glEnableVertexAttribArray(index);
        glVertexAttribPointer(
            index,
            attr.count,
            gl_vertex_type(attr.type),
            attr.normalised ? GL_TRUE : GL_FALSE,
            attr.stride,
            (void*)attr.offset
        );

        index += 1;
    }

    glBindVertexArray(0);
}

VertexArray::VertexArray(VertexArray&& oth)
: m_array(0)
{
    std::swap(m_array, oth.m_array);
    std::swap(m_vertex_count, oth.m_vertex_count);
    std::swap(m_indexed, oth.m_indexed);
}

VertexArray& VertexArray::operator=(VertexArray&& oth)
{
    std::swap(m_array, oth.m_array);
    std::swap(m_vertex_count, oth.m_vertex_count);
    std::swap(m_indexed, oth.m_indexed);
    return *this;
}

VertexArray::~VertexArray()
{
    if (m_array)
    {
        glDeleteVertexArrays(1, &m_array);
    }
}

VertexArrayWithBuffers::VertexArrayWithBuffers(
    utils::VertexFormat const& format,
    utils::Buffer&& index_buffer_,
    utils::Buffer&& vertex_buffer_,
    std::size_t vertex_count
)
: index_buffer(std::move(index_buffer_))
, vertex_buffer(std::move(vertex_buffer_))
, vertex_array(format, index_buffer, vertex_buffer, vertex_count)
{
}

}
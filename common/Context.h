#pragma once


namespace utils
{

class BlendState;
class DepthStencilState;
class RasteriserState;
class Shader;
class VertexArray;

class Context
{
public:
    enum class PrimitiveType
    {
        Lines,
        Triangles
    };


    void bind_blend_state(BlendState const& state);

    void bind_depth_stencil_state(DepthStencilState const& state);

    void bind_rasteriser_state(RasteriserState const& state);

    void bind_shader(Shader const& shader);

    void draw(VertexArray const& vertex_array, PrimitiveType primitive);
};

}
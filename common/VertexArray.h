#pragma once

#include "Buffer.h"

#include <Eigen/Core>
#include <gsl/gsl>

namespace utils
{

class VertexAttribute
{
public:
  enum class Type
  {
      Byte,
      UnsignedByte,
      Short,
      UnsignedShort,
      Int,
      UnsignedInt,
      HalfFloat,
      Float,
      Fixed,
      UnsignedInt_2_10_10_10,
      Int_2_10_10_10
  };

  VertexAttribute(
    Type type,
    std::size_t count,
    std::size_t stride_bytes,
    std::size_t offset_bytes,
    bool normalised=false);

  std::size_t count;
  Type type;
  bool normalised;
  std::size_t stride;
  std::size_t offset;
};

class VertexFormat
{
public:
  VertexFormat(std::initializer_list<VertexAttribute> const& attrs);
  VertexFormat(std::vector<VertexAttribute> const& attrs);

  std::vector<VertexAttribute> const& attributes() const { return m_attributes; }

private:
  std::vector<VertexAttribute> m_attributes;
};

class VertexArray
{
public:
    VertexArray(
        utils::VertexFormat const& format,
        utils::Buffer const& buffer,
        std::size_t vertex_count);

    VertexArray(
        utils::VertexFormat const& format,
        utils::Buffer const& index_buffer,
        utils::Buffer const& buffer,
        std::size_t vertex_count);

    VertexArray(VertexArray&& oth);
    VertexArray& operator=(VertexArray&& oth);
    ~VertexArray();

private:
    std::uint32_t m_array;
    std::size_t m_vertex_count;
    bool m_indexed;

    friend class Context;
};

class VertexArrayWithBuffers
{
public:
    VertexArrayWithBuffers(
        utils::VertexFormat const& format,
        utils::Buffer&& index_buffer,
        utils::Buffer&& buffer,
        std::size_t vertex_count
    );

    Buffer index_buffer;
    Buffer vertex_buffer;
    VertexArray vertex_array;
};

}
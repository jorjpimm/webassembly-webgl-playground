#include "Context.h"
#include "BlendState.h"
#include "DepthStencilState.h"
#include "RasteriserState.h"
#include "Shader.h"
#include "VertexArray.h"

#include <GLES3/gl3.h>

#include <iostream>

namespace utils
{

namespace {
int gl_blend_func(BlendState::BlendMode m)
{
    switch(m)
    {
    case BlendState::BlendMode::Add: return GL_FUNC_ADD;
    case BlendState::BlendMode::Subtract: return GL_FUNC_SUBTRACT;
    case BlendState::BlendMode::ReverseSubtract: return GL_FUNC_REVERSE_SUBTRACT;
    case BlendState::BlendMode::Min: return GL_MIN;
    case BlendState::BlendMode::Max: return GL_MAX;
    }
}

int gl_blend_parameter(BlendState::BlendParameter p)
{
    switch(p)
    {
    case BlendState::BlendParameter::Zero: return GL_ZERO;
    case BlendState::BlendParameter::One: return GL_ONE;
    case BlendState::BlendParameter::SrcColour: return GL_SRC_COLOR;
    case BlendState::BlendParameter::OneMinusSrcColour: return GL_ONE_MINUS_SRC_COLOR;
    case BlendState::BlendParameter::DstColour: return GL_DST_COLOR;
    case BlendState::BlendParameter::OneMinusDstColour: return GL_ONE_MINUS_DST_COLOR;
    case BlendState::BlendParameter::SrcAlpha: return GL_SRC_ALPHA;
    case BlendState::BlendParameter::OneMinusSrcAlpha: return GL_ONE_MINUS_SRC_ALPHA;
    case BlendState::BlendParameter::DstAlpha: return GL_DST_ALPHA;
    case BlendState::BlendParameter::OneMinusDstAlpha: return GL_ONE_MINUS_DST_ALPHA;
    case BlendState::BlendParameter::ConstantColour: return GL_CONSTANT_COLOR;
    case BlendState::BlendParameter::OneMinusConstantColour: return GL_ONE_MINUS_CONSTANT_COLOR;
    case BlendState::BlendParameter::ConstantAlpha: return GL_CONSTANT_ALPHA;
    case BlendState::BlendParameter::OneMinusConstantAlpha: return GL_ONE_MINUS_CONSTANT_ALPHA;
    case BlendState::BlendParameter::SrcAlphaSaturate: return GL_SRC_ALPHA_SATURATE;
    }
}

int gl_depth_func(DepthStencilState::Function m)
{
    switch(m)
    {
    case DepthStencilState::Function::Never: return GL_NEVER;
    case DepthStencilState::Function::Less: return GL_LESS;
    case DepthStencilState::Function::Equal: return GL_LEQUAL;
    case DepthStencilState::Function::LEqual: return GL_GREATER;
    case DepthStencilState::Function::Greater: return GL_GEQUAL;
    case DepthStencilState::Function::NotEqual: return GL_EQUAL;
    case DepthStencilState::Function::GEqual: return GL_NOTEQUAL;
    case DepthStencilState::Function::Always: return GL_ALWAYS;
    }
}
}

void Context::bind_blend_state(BlendState const& state)
{
    if (state.m_enable)
    {
        glEnable(GL_BLEND);

        glBlendEquationSeparate(gl_blend_func(state.m_rgb_mode), gl_blend_func(state.m_alpha_mode));
        glBlendFuncSeparate(
            gl_blend_parameter(state.m_rgb_source),
            gl_blend_parameter(state.m_rgb_dest),
            gl_blend_parameter(state.m_alpha_source),
            gl_blend_parameter(state.m_alpha_dest)
        );
        glBlendColor(
            state.m_blend_colour[0],
            state.m_blend_colour[1],
            state.m_blend_colour[2],
            state.m_blend_colour[3]
        );
    }
    else
    {
        glDisable(GL_BLEND);
    }
}

void Context::bind_depth_stencil_state(DepthStencilState const& state)
{
    using DrawMask = DepthStencilState::DrawMask;
    using Tests = DepthStencilState::Tests;

    glColorMask(
        state.m_write_mask & DrawMask::ColourR,
        state.m_write_mask & DrawMask::ColourG,
        state.m_write_mask & DrawMask::ColourB,
        state.m_write_mask & DrawMask::ColourA
    );

    glDepthMask(state.m_write_mask & DrawMask::Depth);
    glStencilMask(state.m_write_mask & DrawMask::Stencil);

    if (state.m_tests & Tests::DepthTest)
      {
      glEnable(GL_DEPTH_TEST);
      }
    else
      {
      glDisable(GL_DEPTH_TEST);
      }

    if (state.m_tests & Tests::StencilTest)
      {
      glEnable(GL_STENCIL_TEST);
      }
    else
      {
      glDisable(GL_STENCIL_TEST);
      }

    glDepthFunc(gl_depth_func(state.m_depth_test));
    glStencilFunc(gl_depth_func(state.m_stencil_test), state.m_stencil_ref, state.m_stencil_mask);

    glDepthRangef(state.m_depth_near, state.m_depth_far);
}

void Context::bind_rasteriser_state(RasteriserState const& state)
{
    switch(state.m_cull_mode)
    {
    case RasteriserState::CullMode::CullFront:
        glEnable(GL_CULL_FACE);
        glCullFace(GL_FRONT);
    break;
    case RasteriserState::CullMode::CullBack:
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);
        break;
    case RasteriserState::CullMode::CullNone:
    default:
        glDisable(GL_CULL_FACE);
        break;
    }
}

void Context::bind_shader(Shader const& shader)
{
    glUseProgram(shader.m_program);

    for (std::uint32_t i = 0; i < shader.m_resources.size(); ++i)
    {
        auto&& rsc = shader.m_resources[i];
        if (glIsTexture(rsc))
        {
            glActiveTexture(GL_TEXTURE0 + i);
            glBindTexture(GL_TEXTURE_2D, rsc);
        }
    }
}

void Context::draw(VertexArray const& vertex_array, PrimitiveType type)
{
    glBindVertexArray(vertex_array.m_array);

    auto gl_type = GL_TRIANGLES;
    switch (type)
    {
    case PrimitiveType::Lines:
      gl_type = GL_LINES;
      break;
    case PrimitiveType::Triangles:
      gl_type = GL_TRIANGLES;
      break;
    }

    if (vertex_array.m_indexed)
    {
        glDrawElements(
            gl_type,
            vertex_array.m_vertex_count,
            GL_UNSIGNED_SHORT,
            0
        );
    }
    else
    {
        glDrawArrays(
            gl_type,
            0,
            vertex_array.m_vertex_count
        );
    }
}


}
#pragma once

#include <gsl/gsl>

namespace utils
{

class DepthStencilState
{
public:
    enum DrawMask : std::uint8_t
    {
        ColourR = 1,
        ColourG = 2,
        ColourB = 4,
        ColourA = 8,
        Depth = 16,
        Stencil = 32,

        Colour = ColourR | ColourG | ColourB | ColourA,
        MaskAll = Colour | Depth | Stencil
    };

    enum Tests : std::uint8_t
    {
        TestNone = 0,
        DepthTest = 1,
        StencilTest = 2
    };

    enum class Function
    {
        Never,
        Less,
        Equal,
        LEqual,
        Greater,
        NotEqual,
        GEqual,
        Always
    };

    DepthStencilState(
        std::uint16_t write_mask = MaskAll,
        std::uint16_t tests = TestNone,
        Function depth_test = Function::Less,
        Function stencil_test = Function::Always,
        std::uint32_t stencil_ref = 0x0,
        std::uint32_t stencil_mask = 0xFFFFFFFF,
        float depth_near = 0.0f,
        float depth_far = 1.0f
    )
    : m_write_mask(write_mask)
    , m_tests(tests)
    , m_depth_test(depth_test)
    , m_stencil_test(stencil_test)
    , m_stencil_ref(stencil_ref)
    , m_stencil_mask(stencil_mask)
    , m_depth_near(depth_near)
    , m_depth_far(depth_far)
    {
    }


    DepthStencilState(
        std::uint16_t write_mask,
        std::uint16_t tests,
        Function depth_test,
        float depth_near = 0.0f,
        float depth_far = 1.0f
    )
    : DepthStencilState(write_mask, tests, depth_test, Function::Always, 0x0, 0xffffffff, depth_near, depth_far)
    {
        assert(!(tests & StencilTest));
    }

private:
    std::uint16_t m_write_mask;
    std::uint16_t m_tests;
    Function m_depth_test;
    Function m_stencil_test;
    std::uint32_t m_stencil_ref;
    std::uint32_t m_stencil_mask;
    float m_depth_near;
    float m_depth_far;

    friend class Context;
};

}
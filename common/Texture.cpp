#include "Texture.h"

#include <GLES3/gl3.h>

#include <SDL.h>
#include <SDL_image.h>

#include <iostream>

namespace utils
{

namespace {

int invert_image(int width, int height, void *image_pixels) {
    auto temp_row = std::unique_ptr<char>(new char[width]);
    if (temp_row.get() == nullptr) {
        SDL_SetError("Not enough memory for image inversion");
        return -1;
    }
    //if height is odd, don't need to swap middle row
    int height_div_2 = height / 2;
    for (int index = 0; index < height_div_2; index++) {
        //uses string.h
        memcpy((Uint8 *)temp_row.get(),
               (Uint8 *)(image_pixels)+
               width * index,
               width);

        memcpy(
                (Uint8 *)(image_pixels)+
                width * index,
                (Uint8 *)(image_pixels)+
                width * (height - index - 1),
                width);
        memcpy(
                (Uint8 *)(image_pixels)+
                width * (height - index - 1),
                temp_row.get(),
                width);
    }
    return 0;
}
}

Resource::Resource()
: m_resource(0)
{
}

Resource::Resource(Resource&& oth)
: m_resource(0)
{
    std::swap(m_resource, oth.m_resource);
}

Resource& Resource::operator=(Resource&& oth)
{
    std::swap(m_resource, oth.m_resource);
    return *this;
}

Resource::~Resource()
{
    if (glIsTexture(m_resource))
    {
        glDeleteTextures(1, &m_resource);
    }
}

Texture::Texture(gsl::czstring texture_filename)
{
    glGenTextures(1, &m_resource);

    SDL_Surface* loaded_image = { IMG_Load(texture_filename) };
    assert(loaded_image);

    glBindTexture(GL_TEXTURE_2D, m_resource);

    // Enforce RGB/RGBA
    int format;
    SDL_Surface* formattedSurf;
    if (loaded_image->format->BytesPerPixel == 3)
    {
        std::cout << "bbb" << std::endl;
        formattedSurf = SDL_ConvertSurfaceFormat(
            loaded_image,
            SDL_PIXELFORMAT_RGB24,
            0);
        format = GL_RGB;
    }
    else
    {
        std::cout << "AAA" << std::endl;
        formattedSurf = SDL_ConvertSurfaceFormat(
            loaded_image,
            SDL_PIXELFORMAT_BGRA5551,
            0);
        format = GL_RGBA;
    }

    if(formattedSurf == nullptr)
    {
        std::cout << "Unable to convert loaded surface to display format! SDL Error: " << SDL_GetError() << std::endl;
    }

    std::cout << formattedSurf << " " << formattedSurf->w << " " << formattedSurf->h << " " << formattedSurf->pixels << std::endl;

    invert_image(
        formattedSurf->w*formattedSurf->format->BytesPerPixel,
        formattedSurf->h,
        (char *)formattedSurf->pixels
    );

    /* Generate The Texture */
    glTexImage2D(
        GL_TEXTURE_2D,
        0,
        format,
        loaded_image->w,
        loaded_image->h,
        0,
        format,
        GL_UNSIGNED_BYTE,
        formattedSurf->pixels
    );

    std::cout << formattedSurf->w << " " << formattedSurf->h << std::endl;

    /* Linear Filtering */
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    SDL_FreeSurface(formattedSurf);
    SDL_FreeSurface(loaded_image);

    glBindTexture(GL_TEXTURE_2D, 0);
}

}
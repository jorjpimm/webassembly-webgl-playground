#include "Modeller.h"
#include "Buffer.h"
#include "VertexArray.h"

#include <iostream>

namespace utils
{

template <typename T, typename Y>
std::vector<T>& operator<<(std::vector<T>& vec, Y const& tish)
{
  vec.push_back(tish);
  return vec;
}

Modeller::Modeller()
: m_transform(Eigen::Affine3f::Identity())
{
    m_vertex.reserve(128);
    m_texture.reserve(128);
    m_normals.reserve(128);
    m_colours.reserve(128);

    m_are_triangle_indices_sequential = m_are_line_indices_sequential = true;

    save();
}

Modeller::~Modeller()
{
}

namespace {
template <typename T>
    static void bake_array(
        std::vector<gsl::byte>& data,
        std::size_t offset,
        std::size_t stride,
        std::vector<T> const& data_in)
{
    std::size_t count = data_in.size();

    assert((offset + (stride * (count-1))) <= data.size());
    gsl::byte *data_out = data.data();
    for(std::size_t i = 0; i < count; ++i)
    {
        gsl::byte *d = data_out + offset + (stride * i);
        std::memcpy(d, data_in[i].data(), sizeof(T));
    }
}
std::size_t semantic_size(Modeller::Semantic s)
{
  switch(s)
  {
    case Modeller::Semantic::Position: return 3;
    case Modeller::Semantic::Colour: return 4;
    case Modeller::Semantic::TextureCoordinate: return 2;
    case Modeller::Semantic::Normal: return 3;
  }
}
}

void Modeller::bake_vertices(
    gsl::span<const Semantic> const& semantics,
    gsl::not_null<VertexFormat*> format_out,
    gsl::not_null<Buffer*> vertex_data,
    std::size_t* vertex_count) const
{
    std::size_t vert_size = 0;
    for (auto semantic : semantics)
    {
        vert_size += semantic_size(semantic);
    }
    vert_size *= sizeof(float);

    std::vector<gsl::byte> data(m_vertex.size() * vert_size);
    std::vector<VertexAttribute> attributes;

    std::size_t offset = 0;
    for (auto semantic : semantics)
    {
        std::size_t attr_size = semantic_size(semantic);
        attributes.push_back(VertexAttribute(VertexAttribute::Type::Float, attr_size, vert_size, offset));
        if(semantic == Modeller::Semantic::Position)
        {
            bake_array(data, offset, vert_size, m_vertex);
            offset += sizeof(float) * attr_size;
        }
        else if(semantic == Modeller::Semantic::Normal)
        {
            bake_array(data, offset, vert_size, m_normals);
            offset += sizeof(float) * attr_size;
        }
        else if(semantic == Modeller::Semantic::Colour)
        {
            bake_array(data, offset, vert_size, m_colours);
            offset += sizeof(float) * attr_size;
        }
        else if(semantic == Modeller::Semantic::TextureCoordinate)
        {
            bake_array(data, offset, vert_size, m_texture);
            offset += sizeof(float) * attr_size;
        }
    }

    *vertex_count = data.size() / vert_size;
    assert((data.size() % vert_size) == 0);

    *format_out = VertexFormat(attributes);


    vertex_data->set_data(gsl::make_span(data));
}

VertexArrayWithBuffers Modeller::bake_triangles(
    gsl::span<const Semantic> const& semantics
) const
{
    std::size_t vertex_count = 0;
    auto vertex_buffer = utils::Buffer(utils::Buffer::Type::VertexArray);
    auto index_buffer = utils::Buffer(utils::Buffer::Type::IndexArray);
    utils::VertexFormat vertex_format{};

    bake_triangles(
        semantics,
        &vertex_format,
        &index_buffer,
        &vertex_buffer,
        &vertex_count);

    return VertexArrayWithBuffers(
        vertex_format,
        std::move(index_buffer),
        std::move(vertex_buffer),
        vertex_count
    );
}

VertexArrayWithBuffers Modeller::bake_lines(
    gsl::span<const Semantic> const& semantics
) const
{
    std::size_t vertex_count = 0;
    auto vertex_buffer = utils::Buffer(utils::Buffer::Type::VertexArray);
    auto index_buffer = utils::Buffer(utils::Buffer::Type::IndexArray);
    utils::VertexFormat vertex_format{};

    bake_lines(
        semantics,
        &vertex_format,
        &index_buffer,
        &vertex_buffer,
        &vertex_count);

    return VertexArrayWithBuffers(
        vertex_format,
        std::move(index_buffer),
        std::move(vertex_buffer),
        vertex_count
    );
}

void Modeller::bake_triangles(
    gsl::span<const Semantic> const& semantics,
    gsl::not_null<VertexFormat*> format_out,
    gsl::not_null<Buffer*> index_data_out,
    gsl::not_null<Buffer*> vertex_data_out,
    gsl::not_null<std::size_t*> vertex_count_out) const
{
    assert(index_data_out->type() == utils::Buffer::Type::IndexArray);
    assert(vertex_data_out->type() == utils::Buffer::Type::VertexArray);

    std::size_t non_indexed_count = 0;
    bake_vertices(semantics, format_out, vertex_data_out, &non_indexed_count);

    assert(m_tri_indices.size() < std::numeric_limits<std::uint16_t>::max());
    index_data_out->set_data(gsl::make_span(m_tri_indices).as_bytes());
    *vertex_count_out = m_tri_indices.size();
}

void Modeller::bake_lines(
    gsl::span<const Semantic> const& semantics,
    gsl::not_null<VertexFormat*> format_out,
    gsl::not_null<Buffer*> index_data_out,
    gsl::not_null<Buffer*> vertex_data_out,
    gsl::not_null<std::size_t*> vertex_count_out) const
{
    assert(index_data_out->type() == utils::Buffer::Type::IndexArray);
    assert(vertex_data_out->type() == utils::Buffer::Type::VertexArray);
    std::size_t non_indexed_count = 0;
    bake_vertices(semantics, format_out, vertex_data_out, &non_indexed_count);

    assert(m_line_indices.size() < std::numeric_limits<std::uint16_t>::max());
    index_data_out->set_data(gsl::make_span(m_line_indices).as_bytes());
    *vertex_count_out = m_line_indices.size();
}

void Modeller::begin(Type type)
  {
  m_quad_count = 0;
  m_states.back().type = type;
  }

void Modeller::end()
  {
  m_states.back().type = None;
  }

void Modeller::vertex(Eigen::Vector3f const& vec)
{
    if(m_normals.size() || !m_states.back().normal.isZero())
    {
        while(m_normals.size() < m_vertex.size())
        {
            m_normals.push_back(Eigen::Vector3f::Zero());
        }
        m_normals.push_back(transform_normal(m_states.back().normal));
    }

    if(m_texture.size() || !m_states.back().texture.isZero())
    {
        while(m_texture.size() < m_vertex.size() )
        {
            m_texture.push_back(Eigen::Vector2f::Zero());
        }
        m_texture.push_back(m_states.back().texture);
    }

    if(m_colours.size() || !m_states.back().colour.isZero() )
    {
        while(m_colours.size() < m_vertex.size() )
        {
            m_colours.push_back(Eigen::Vector4f::Zero());
        }
        m_colours.push_back(m_states.back().colour);
    }

    m_vertex << transform_point( vec );

    if(m_states.back().type == Lines)
    {
        m_are_line_indices_sequential |= m_line_indices.size() == m_vertex.size();
        m_line_indices << (std::uint16_t)(m_vertex.size() - 1);
    }
    else if(m_states.back().type == Triangles)
    {
        m_are_triangle_indices_sequential |= m_tri_indices.size() == m_vertex.size();
        m_tri_indices << (std::uint16_t)(m_vertex.size() - 1);

        if(m_states.back().normals_automatic && (m_tri_indices.size() % 3) == 0)
        {
            m_are_triangle_indices_sequential = false;
            std::size_t i1( m_vertex.size() - 3 );
            std::size_t i2( m_vertex.size() - 2 );
            std::size_t i3( m_vertex.size() - 1 );
            while(m_normals.size() < m_vertex.size())
            {
                m_normals.push_back(Eigen::Vector3f::Zero());
            }
            Eigen::Vector3f vec1(m_vertex[i2] - m_vertex[i1]);
            Eigen::Vector3f vec2(m_vertex[i3] - m_vertex[i1]);

            m_normals[i1] = m_normals[i2] = m_normals[i3] = vec1.cross(vec2).normalized();
        }
    }
    else if(m_states.back().type == Quads)
    {
        m_quad_count++;
        m_are_triangle_indices_sequential = false;
        m_tri_indices.push_back((std::uint16_t)(m_vertex.size() - 1));

        if(m_quad_count == 4)
        {
            if(m_states.back().normals_automatic)
            {
                std::size_t i1(m_vertex.size() - 4);
                std::size_t i2(m_vertex.size() - 3);
                std::size_t i3(m_vertex.size() - 2);
                std::size_t i4(m_vertex.size() - 1);
                while(m_normals.size() < m_vertex.size())
                {
                    m_normals.push_back(Eigen::Vector3f::Zero());
                }
                Eigen::Vector3f vec1(m_vertex[i2] - m_vertex[i1]);
                Eigen::Vector3f vec2(m_vertex[i3] - m_vertex[i1]);

                m_normals[i1] = m_normals[i2] = m_normals[i3] = m_normals[i4] = vec1.cross(vec2).normalized();
            }

            std::size_t idxA = m_tri_indices.size()-4;
            std::size_t idxB = m_tri_indices.size()-2;
            m_tri_indices.push_back(m_tri_indices[idxA]);
            m_tri_indices.push_back(m_tri_indices[idxB]);
            m_quad_count = 0;
        }
    }
}

void Modeller::normal(const Eigen::Vector3f &norm)
{
    m_states.back().normal = norm;
}

void Modeller::texture(const Eigen::Vector2f &tex)
{
    m_states.back().texture = tex;
}

void Modeller::colour(const Eigen::Vector4f &col)
{
    m_states.back().colour = col;
}

void Modeller::set_normals_automatic(bool normals_automatic)
{
    m_states.back().normals_automatic = normals_automatic;
    if(normals_automatic)
    {
        m_states.back().normal = Eigen::Vector3f::Zero();
    }
}

bool Modeller::are_normals_automatic() const
{
    return m_states.back().normals_automatic;
}

void Modeller::draw_wire_cube(Eigen::Vector3f const& min, Eigen::Vector3f const& size)
{
    m_are_line_indices_sequential = false;

    std::uint16_t sI = (std::uint16_t)m_vertex.size();

    m_vertex.push_back(min);
    m_vertex.push_back(min + Eigen::Vector3f(size.x(), 0.0f, 0.0f));
    m_vertex.push_back(min + Eigen::Vector3f(size.x(), size.y(), 0.0f));
    m_vertex.push_back(min + Eigen::Vector3f(0.0f, size.y(), 0.0f));
    m_vertex.push_back(min + Eigen::Vector3f(0.0f, 0.0f, size.z()));
    m_vertex.push_back(min + Eigen::Vector3f(size.x(), 0.0f, size.z()));
    m_vertex.push_back(min + size);
    m_vertex.push_back(min + Eigen::Vector3f(0.0f, size.y(), size.z()));

    Eigen::Vector3f n(Eigen::Vector3f::Zero());
    Eigen::Vector2f t(Eigen::Vector2f::Zero());
    for (std::size_t i = 0; i < 8; ++i)
    {
        m_normals.push_back(n);
        m_texture.push_back(t);
    }

    m_line_indices.push_back(sI);     m_line_indices.push_back(sI+1);
    m_line_indices.push_back(sI+1);   m_line_indices.push_back(sI+2);
    m_line_indices.push_back(sI+2);   m_line_indices.push_back(sI+3);
    m_line_indices.push_back(sI+3);   m_line_indices.push_back(sI);

    m_line_indices.push_back(sI+4);   m_line_indices.push_back(sI+5);
    m_line_indices.push_back(sI+5);   m_line_indices.push_back(sI+6);
    m_line_indices.push_back(sI+6);   m_line_indices.push_back(sI+7);
    m_line_indices.push_back(sI+7);   m_line_indices.push_back(sI+4);

    m_line_indices.push_back(sI+4);   m_line_indices.push_back(sI);
    m_line_indices.push_back(sI+5);   m_line_indices.push_back(sI+1);
    m_line_indices.push_back(sI+6);   m_line_indices.push_back(sI+2);
    m_line_indices.push_back(sI+7);   m_line_indices.push_back(sI+3);
}

void Modeller::draw_wire_circle(
    const Eigen::Vector3f &pos,
    const Eigen::Vector3f &normal,
    float radius,
    std::size_t pts)
{
    m_are_line_indices_sequential = false;

    Eigen::Vector3f up = Eigen::Vector3f(0,1,0);
    if(normal.dot(up) > 0.9f)
    {
        up = Eigen::Vector3f(1, 0, 0);
    }
    Eigen::Vector3f x = up.cross(normal);
    Eigen::Vector3f y = normal.cross(x);

    std::uint16_t initialIndex = (std::uint16_t)m_vertex.size();
    for (std::uint16_t i = 0; i < (std::uint16_t)pts; ++i)
    {
        float angle = i * (M_PI * 2.0f / (float)pts);
        std::uint16_t otherIndex = (std::uint16_t)(i+1) % pts;

        m_normals.push_back(Eigen::Vector3f::Zero());
        m_texture.push_back(Eigen::Vector2f::Zero());
        m_vertex.push_back(pos + (radius * (x * sinf(angle) + y * cosf(angle))));
        m_line_indices.push_back(initialIndex + i);
        m_line_indices.push_back(initialIndex + otherIndex);
    }
}

void Modeller::draw_sphere(float r, int lats, int longs)
{
    int i, j;
    for (i = 0; i < lats; i++)
    {
        float lat0 = M_PI * (-0.5 + (float)i / lats);
        float z0  = sinf(lat0) * r;
        float zr0 = cosf(lat0) * r;

        float lat1 = M_PI * (-0.5 + (float)(i+1) / lats);
        float z1 = sinf(lat1) * r;
        float zr1 = cosf(lat1) * r;

        float vA = (float)i / (float)(lats);
        float vB = (float)(i+1) / (float)(lats);

        bool beginCap = i == 0;
        bool endCap = i == (lats - 1);

        begin((beginCap || endCap) ? Triangles : Quads);
        for (j = 0; j < longs; j++)
        {
            float lng = 2 * M_PI * (float) (j - 1) / longs;
            float x = cosf(lng);
            float y = sinf(lng);

            float lngOld = 2 * M_PI * (float) (j - 2) / longs;
            float xOld = cosf(lngOld);
            float yOld = sinf(lngOld);

            float uA = (float)(j) / (float)(longs);
            float uB = (float)(j+1) / (float)(longs);

            texture(uA, vB);
            normal(Eigen::Vector3f(xOld * zr1, yOld * zr1, z1).normalized());
            vertex(xOld * zr1, yOld * zr1, z1);

            if (!beginCap)
            {
                texture(uA, vA);
                normal(Eigen::Vector3f(xOld * zr0, yOld * zr0, z0).normalized());
                vertex(xOld * zr0, yOld * zr0, z0);
            }

            texture(uB, vA);
            normal(Eigen::Vector3f(x * zr0, y * zr0, z0).normalized());
            vertex(x * zr0, y * zr0, z0);

            if (!endCap)
            {
                texture(uB, vB);
                normal(Eigen::Vector3f(x * zr1, y * zr1, z1).normalized());
                vertex(x * zr1, y * zr1, z1);
            }
        }
        end();
    }
}

void Modeller::draw_cube(
    const Eigen::Vector3f &hor,
    const Eigen::Vector3f &ver,
    const Eigen::Vector3f &dep,
    float pX,
    float pY)
{
    m_are_triangle_indices_sequential = false;

    Eigen::Vector3f h = hor * 0.5f;
    Eigen::Vector3f v = ver * 0.5f;
    Eigen::Vector3f d = dep * 0.5f;

    Eigen::Vector3f p1(transform_point(-h-v-d)),
        p2(transform_point(h-v-d)),
        p3(transform_point(h+v-d)),
        p4(transform_point(-h+v-d)),
        p5(transform_point(-h-v+d)),
        p6(transform_point(h-v+d)),
        p7(transform_point(h+v+d)),
        p8(transform_point(-h+v+d));

    Eigen::Vector3f n1(transform_normal(Eigen::Vector3f(0,1,0))),
        n2(transform_normal(Eigen::Vector3f(0,-1,0))),
        n3(transform_normal(Eigen::Vector3f(1,0,0))),
        n4(transform_normal(Eigen::Vector3f(-1,0,0))),
        n5(transform_normal(Eigen::Vector3f(0,0,-1))),
        n6(transform_normal(Eigen::Vector3f(0,0,1)));

    // Top Face BL
    {
    std::uint16_t begin = (std::uint16_t)m_vertex.size();
    m_tri_indices << begin << begin + 1 << begin + 2 << begin + 2 << begin + 1 << begin + 3;

    m_normals << n1 << n1 << n1 << n1;
    m_texture << Eigen::Vector2f(1.0/3.0,pY) << Eigen::Vector2f(1.0/3.0,0.5-pY) << Eigen::Vector2f(pX,pY) << Eigen::Vector2f(pX,0.5-pY);
    m_vertex << p3 << p4 << p7 << p8;
    }

    // Back Face BM
    {
    std::uint16_t begin = (std::uint16_t) m_vertex.size();
    m_tri_indices << begin << begin + 1 << begin + 2 << begin + 2 << begin + 1 << begin + 3;

    m_normals << n5 << n5 << n5 << n5;
    m_texture << Eigen::Vector2f(2.0/3.0,pY) << Eigen::Vector2f(2.0/3.0,0.5-pY) << Eigen::Vector2f(1.0/3.0,pY) << Eigen::Vector2f(1.0/3.0,0.5-pY);
    m_vertex << p2 << p1 << p3 << p4;
    }

    // Bottom Face BR
    {
    std::uint16_t begin = (std::uint16_t)m_vertex.size();
    m_tri_indices << begin << begin + 1 << begin + 2 << begin + 2 << begin + 1 << begin + 3;

    m_normals << n2 << n2 << n2 << n2;
    m_texture << Eigen::Vector2f(2.0/3.0,0.5-pY) << Eigen::Vector2f(2.0/3.0,pY) << Eigen::Vector2f(1-pX,0.5-pY) << Eigen::Vector2f(1-pX,pY);
    m_vertex << p1 << p2 << p5 << p6;
    }

    // Left Face TL
    {
    std::uint16_t begin = (std::uint16_t)m_vertex.size();
    m_tri_indices << begin << begin + 1 << begin + 2 << begin + 2 << begin + 1 << begin + 3;

    m_normals << n3 << n3 << n3 << n3;
    m_texture << Eigen::Vector2f(1-pX,0.5+pY) << Eigen::Vector2f(1-pX,1-pY) << Eigen::Vector2f(2.0/3.0,0.5+pY) << Eigen::Vector2f(2.0/3.0,1-pY);
    m_vertex << p2 << p3 << p6 << p7;
    }

    // Front Face TM
    {
    std::uint16_t begin = (std::uint16_t)m_vertex.size();
    m_tri_indices << begin << begin + 1 << begin + 2 << begin + 2 << begin + 1 << begin + 3;

    m_normals << n6 << n6 << n6 << n6;
    m_texture << Eigen::Vector2f(1.0/3.0,0.5+pY) << Eigen::Vector2f(2.0/3.0,0.5+pY) << Eigen::Vector2f(1.0/3.0,1-pY) << Eigen::Vector2f(2.0/3.0,1-pY);
    m_vertex << p5 << p6 << p8 << p7;
    }

    // Right Face TR
    {
    std::uint16_t begin = (std::uint16_t)m_vertex.size();
    m_tri_indices << begin << begin + 1 << begin + 2 << begin + 2 << begin + 1 << begin + 3;

    m_normals << n4 << n4 << n4 << n4;
    m_texture << Eigen::Vector2f(pX,1-pY) << Eigen::Vector2f(pX,0.5+pY) << Eigen::Vector2f(1.0/3.0,1-pY) << Eigen::Vector2f(1.0/3.0,0.5+pY);
    m_vertex << p4 << p1 << p8 << p5;
    }
}

void Modeller::draw_quad(const Eigen::Vector3f &hor, const Eigen::Vector3f &ver)
{
    m_are_triangle_indices_sequential = false;
    Eigen::Vector3f h = hor / 2.0;
    Eigen::Vector3f v = ver / 2.0;

    std::uint16_t begin = (std::uint16_t)m_vertex.size();
    m_tri_indices << begin << begin + 1 << begin + 2 << begin << begin + 2 << begin + 3;
    m_vertex << transform_point( -h - v ) << transform_point( h - v ) << transform_point( h + v ) << transform_point( -h + v );
    m_texture << Eigen::Vector2f(0,0) << Eigen::Vector2f(1,0) << Eigen::Vector2f(1,1) << Eigen::Vector2f(0,1);

    Eigen::Vector3f norm( transform_normal( h.cross(v).normalized() ) );
    m_normals << norm << norm << norm << norm;
}

void Modeller::draw_locator(const Eigen::Vector3f &size, const Eigen::Vector3f &center)
{
    std::uint16_t begin = (std::uint16_t)m_vertex.size();
    m_line_indices << begin << begin + 1 << begin + 2 << begin + 3 << begin + 4 << begin + 5;

    m_vertex << transform_point( center + Eigen::Vector3f( -size.x(), 0, 0 ) )
            << transform_point( center + Eigen::Vector3f( size.x(), 0, 0 ) )
            << transform_point( center + Eigen::Vector3f( 0, -size.y(), 0 ) )
            << transform_point( center + Eigen::Vector3f( 0, size.y(), 0 ) )
            << transform_point( center + Eigen::Vector3f( 0, 0, -size.z() ) )
            << transform_point( center + Eigen::Vector3f( 0, 0, size.z() ) );

    m_texture << Eigen::Vector2f() << Eigen::Vector2f() << Eigen::Vector2f() << Eigen::Vector2f() << Eigen::Vector2f() << Eigen::Vector2f();
    m_normals << Eigen::Vector3f() << Eigen::Vector3f() << Eigen::Vector3f() << Eigen::Vector3f() << Eigen::Vector3f() << Eigen::Vector3f();
}

void Modeller::set_transform( const Eigen::Affine3f &t )
{
    m_transform = t;
}

Eigen::Affine3f Modeller::transform( ) const
{
    return m_transform;
}

void Modeller::save()
{
    m_states << State();
}

void Modeller::restore()
{
    if (m_states.size() > 1)
    {
        m_states.pop_back();
    }
}

Eigen::Vector3f Modeller::transform_point( const Eigen::Vector3f &in )
{
    return m_transform * in;
}

void Modeller::transform_points(std::vector<Eigen::Vector3f> &list)
{
    if (m_transform.isApprox(Eigen::Affine3f::Identity()))
    {
        return;
    }

    for (std::size_t i = 0, s = list.size(); i < s; ++i)
    {
        Eigen::Vector3f& v = list[i];
        Eigen::Vector3f tr = m_transform * v;
        v = tr;
    }
}

Eigen::Vector3f Modeller::transform_normal(Eigen::Vector3f const& in)
{
    return m_transform.linear() * in;
}

void Modeller::transform_normals(std::vector<Eigen::Vector3f> &list, bool reNormalize)
{
    if (m_transform.isApprox(Eigen::Affine3f::Identity()))
    {
        return;
    }

    if (reNormalize)
    {
        for (std::size_t i = 0, s = list.size(); i < s; ++i)
        {
            Eigen::Vector3f& v = list[i];
            Eigen::Vector3f tr = (m_transform.linear() * v).normalized();
            v = tr;
        }
    }
    else
    {
        for (std::size_t i = 0, s = list.size(); i < s; ++i)
        {
            Eigen::Vector3f& v = list[i];
            Eigen::Vector3f tr = m_transform.linear() * v;
            v = tr;
        }
    }
}

}

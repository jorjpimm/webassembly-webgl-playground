#include "Shader.h"
#include "Buffer.h"
#include "Texture.h"

#include <GLES3/gl3.h>

#include <fstream>
#include <iostream>

namespace {

static int check_linked(int program)
{
    GLint success = 0;
    glGetProgramiv(program, GL_LINK_STATUS, &success);

    if (success == GL_FALSE)
    {
        GLint max_len = 0;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &max_len);

        GLchar err_log[max_len];
        glGetProgramInfoLog(program, max_len, &max_len, &err_log[0]);

        std::cout << "Program linking failed: " << err_log << std::endl;
    }

    return success;
}
}

namespace utils {

std::uint32_t compile_shader(int shader_type, gsl::czstring filename)
{
    std::ifstream input(filename, std::ios::binary);
    assert(input.good());

    // copies all data into buffer
    std::vector<char> buffer{
        std::istreambuf_iterator<char>(input),
        std::istreambuf_iterator<char>()
    };
    buffer.push_back('\0');

    const char *strs[] =
    {
        buffer.data(),
    };

    std::uint32_t shader = glCreateShader(shader_type);
    glShaderSource(shader, 1, strs, NULL);
    glCompileShader(shader);

    GLint success = 0;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);

    if (success == GL_FALSE)
    {
        GLint max_len = 0;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &max_len);

        GLchar err_log[max_len];
        glGetShaderInfoLog(shader, max_len, &max_len, &err_log[0]);
        glDeleteShader(shader);

        std::cout << "Shader compilation failed: " << err_log << std::endl;
        return 0;
    }

    return shader;
}

Shader::Shader(gsl::czstring vertex_filename, gsl::czstring fragment_filename)
{
    m_vertex_shader = compile_shader(GL_VERTEX_SHADER, vertex_filename);
    m_fragment_shader = compile_shader(GL_FRAGMENT_SHADER, fragment_filename);

    m_program = glCreateProgram();
    glAttachShader(m_program, m_vertex_shader);
    glAttachShader(m_program, m_fragment_shader);
    glLinkProgram(m_program);
    check_linked(m_program);
}

Shader::Shader(Shader&& oth)
: m_program(0)
, m_fragment_shader(0)
, m_vertex_shader(0)
{
    std::swap(m_program, oth.m_program);
    std::swap(m_fragment_shader, oth.m_fragment_shader);
    std::swap(m_vertex_shader, oth.m_vertex_shader);
}

Shader& Shader::operator=(Shader&& oth)
{
    std::swap(m_program, oth.m_program);
    std::swap(m_fragment_shader, oth.m_fragment_shader);
    std::swap(m_vertex_shader, oth.m_vertex_shader);
    return *this;
}

Shader::~Shader()
{
    if (m_program)
    {
        glDeleteProgram(m_program);
    }

    if (m_fragment_shader)
    {
        glDeleteShader(m_fragment_shader);
    }

    if (m_vertex_shader)
    {
        glDeleteShader(m_vertex_shader);
    }
}

void Shader::set_uniform(
    std::uint32_t index,
    Buffer const* buffer)
{
    assert(index < 32);
    char str[64];
    sprintf(str, "uniform_%d", index);

    glUseProgram(m_program);

    auto block_index = glGetUniformBlockIndex(m_program, str);
    assert (block_index != GL_INVALID_INDEX);
    glUniformBlockBinding(m_program, block_index, index);

    glBindBufferBase(GL_UNIFORM_BUFFER, index, buffer->m_buffer);
    glUseProgram(0);
}

void Shader::set_resources(
    std::uint32_t first,
    gsl::span<Resource const*> data
)
{
    auto last_rsc = first+data.size();
    assert(last_rsc < 32);

    if (m_resources.size() <= last_rsc)
    {
        m_resources.resize(last_rsc);
    }

    glUseProgram(m_program);
    for (std::uint32_t index = 0; index < data.size(); ++index)
    {
        char str[64];
        sprintf(str, "resource_%d", index);

        std::uint32_t bind_index = first + index;

        auto location = glGetUniformLocation(m_program, str);
        glUniform1i(location, bind_index);
        m_resources[bind_index] = data[index]->m_resource;
    }
    glUseProgram(0);
}

}

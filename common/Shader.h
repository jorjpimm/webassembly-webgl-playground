#pragma once

#include <Eigen/Core>
#include <gsl/gsl>

namespace utils
{

class Buffer;
class Resource;

class Shader
{
public:
    Shader(gsl::czstring vertex_shader_filename, gsl::czstring fragment_shader_filename);
    Shader(Shader&& oth);
    Shader& operator=(Shader&& oth);
    ~Shader();

    void set_uniform(std::uint32_t index, Buffer const* buffer);

    void set_resource(
        std::uint32_t first,
        Resource const& data
    )
    {
        set_resource(first, &data);
    }

    void set_resource(
        std::uint32_t first,
        Resource const* data
    )
    {
        auto span = gsl::make_span(&data, 1);
        set_resources(first, span);
    }

    void set_resources(
        std::uint32_t first,
        gsl::span<Resource const*> data
    );

private:
    std::uint32_t m_vertex_shader;
    std::uint32_t m_fragment_shader;
    std::uint32_t m_program;
    std::vector<std::uint32_t> m_resources;

    friend class Context;
};


}
#include "math_utils.h"

namespace utils
{

Eigen::Matrix4f perspective_projection_matrix(
    float y_fov,
    float aspect_ratio,
    float z_near,
    float z_far)
{
    auto y_scale = std::cosf(y_fov/2) / std::sinf(y_fov/2);
    auto x_scale = y_scale/aspect_ratio;

    Eigen::Matrix4f proj = Eigen::Matrix4f::Identity();
    proj << x_scale,    0,          0,                                  0,
            0,          y_scale,    0,                                  0,
            0,          0,          -z_far/(z_far-z_near),              -1,
            0,          0,          -z_far * z_near / (z_far - z_near), 0;

    return proj;
}

Eigen::Matrix4f ortho_projection_matrix(
    float left,
    float right,
    float bottom,
    float top,
    float z_near,
    float z_far)
{
    Eigen::Matrix4f proj;
    proj <<     (2)/(right-left),   0,                  0,                      -((right+left)/(right-left)),
                0,                  (2)/(top-bottom),   0,                      -((top+bottom)/(top-bottom)),
                0,                  0,                  (-2)/(z_far-z_near),    -((z_far+z_near)/(z_far-z_near)),
                0,                  0,                  0,                      1;
    return proj;
}


Eigen::Affine3f look_at(
    const Eigen::Vector3f &eye,
    const Eigen::Vector3f &aim,
    const Eigen::Vector3f &up)
{
    Eigen::Vector3f forward = (aim - eye).normalized();
    Eigen::Vector3f side = forward.cross(up).normalized();
    Eigen::Vector3f up_vector = side.cross(forward);

    Eigen::Affine3f m = Eigen::Affine3f::Identity();

    m.matrix().row(0).head<3>() = side;
    m.matrix().row(1).head<3>() = up_vector;
    m.matrix().row(2).head<3>() = -forward;

    m.translate(-eye);

    return m;
}

}
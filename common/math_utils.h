#pragma once

#include <Eigen/Geometry>

namespace utils
{

Eigen::Matrix4f perspective_projection_matrix(
    float y_fov,
    float aspect_ratio,
    float z_near,
    float z_far
);

Eigen::Matrix4f ortho_projection_matrix(
    float left,
    float right,
    float bottom,
    float top,
    float z_near,
    float z_far
);

Eigen::Affine3f look_at(
    const Eigen::Vector3f &eye,
    const Eigen::Vector3f &aim,
    const Eigen::Vector3f &up
);

}
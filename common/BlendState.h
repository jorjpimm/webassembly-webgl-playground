#pragma once

#include <Eigen/Core>
#include <gsl/gsl>

namespace utils
{

class BlendState
{
public:
    enum class BlendMode
    {
        Add,
        Subtract,
        ReverseSubtract,
        Min,
        Max
    };

    enum class BlendParameter
    {
        Zero,
        One,
        SrcColour,
        OneMinusSrcColour,
        DstColour,
        OneMinusDstColour,
        SrcAlpha,
        OneMinusSrcAlpha,
        DstAlpha,
        OneMinusDstAlpha,
        ConstantColour,
        OneMinusConstantColour,
        ConstantAlpha,
        OneMinusConstantAlpha,
        SrcAlphaSaturate,
    };

    BlendState(
        bool enable = false,
        BlendMode rgb_mode = BlendMode::Add,
        BlendParameter rgb_source = BlendParameter::SrcAlpha,
        BlendParameter rgb_dest = BlendParameter::OneMinusSrcAlpha,
        BlendMode alpha_mode = BlendMode::Add,
        BlendParameter alpha_source = BlendParameter::SrcAlpha,
        BlendParameter alpha_dest = BlendParameter::OneMinusSrcAlpha,
        Eigen::Vector4f const& blend_colour = Eigen::Vector4f::Zero()
    )
    : m_enable(enable)
    , m_rgb_mode(rgb_mode)
    , m_rgb_source(rgb_source)
    , m_rgb_dest(rgb_dest)
    , m_alpha_mode(alpha_mode)
    , m_alpha_source(alpha_source)
    , m_alpha_dest(alpha_dest)
    , m_blend_colour(blend_colour)
    {
    }

private:
    bool m_enable;
    BlendMode m_rgb_mode;
    BlendParameter m_rgb_source;
    BlendParameter m_rgb_dest;
    BlendMode m_alpha_mode;
    BlendParameter m_alpha_source;
    BlendParameter m_alpha_dest;
    Eigen::Vector4f m_blend_colour;

    friend class Context;
};

}